# Changelog

Todas as mudanças notáveis feitas neste projeto serão documentadas neste arquivo.

O formato é baseado em [Keep a Changelog](https://keepachangelog.com/pt-BR/1.0.0/), e este projeto segue [Semantic Versioning](https://semver.org/).

## [1.1.0] - 2024-05-04
### Adicionado
- Novo exemplo para o padrão "Factory Method".
- Instruções para rodar scripts de padrões específicos usando o script Bash.

### Modificado
- Atualizada a seção de "Como usar" no README.md.
- Melhorias na estrutura do projeto para separar padrões por categorias.

### Corrigido
- Correção de erro ao rodar script de execução de padrões.

## [1.0.0] - 2024-05-01
### Adicionado
- Criado repositório inicial com exemplos de padrões de design criacionais, estruturais e comportamentais.
- Adicionado suporte a TypeScript, ts-node e nodemon para desenvolvimento e execução.

