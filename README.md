# Repositório de Padrões de Design com TypeScript

Bem-vindo ao repositório de **Padrões de Design com TypeScript**! Este repositório contém exemplos práticos de diversos padrões de design implementados em TypeScript. Se você é desenvolvedor de software, estudante ou simplesmente interessado em aprender sobre padrões de design, este repositório é para você.

## O Que São Padrões de Design?

Padrões de design são soluções comuns para problemas recorrentes no design de software. Eles fornecem uma maneira padronizada de abordar problemas comuns na construção de sistemas, promovendo boas práticas e facilitando a manutenção e expansão do código.

## Padrões de Design Disponíveis

Aqui está uma lista dos padrões de design que você encontrará neste repositório:

- **Criacionais**
  - [Factory Method](./src/creational/factory-method)
  - [Abstract Factory](./src/creational/abstract-factory)
  - [Singleton](./src/creational/singleton)
  - [Builder](./src/creational/builder)
  - [Prototype](./src/creational/prototype)

- **Estruturais**
  - [Adapter](./src/structural/adapter)
  - [Bridge](./src/structural/bridge)
  - [Composite](./src/structural/composite)
  - [Decorator](./src/structural/decorator)
  - [Facade](./src/structural/facade)
  - [Flyweight](./src/structural/flyweight)
  - [Proxy](./src/structural/proxy)

- **Comportamentais**
  - [Chain of Responsibility](./src/behavioral/chain-of-responsibility)
  - [Command](./src/behavioral/command)
  - [Interpreter](./src/behavioral/interpreter)
  - [Iterator](./src/behavioral/iterator)
  - [Mediator](./src/behavioral/mediator)
  - [Memento](./src/behavioral/memento)
  - [Observer](./src/behavioral/observer)
  - [State](./src/behavioral/state)
  - [Strategy](./src/behavioral/strategy)
  - [Template Method](./src/behavioral/template-method)
  - [Visitor](./src/behavioral/visitor)

## Como Usar Este Repositório

1. Clone o repositório para o seu ambiente local:
   ```bash
   git clone https://github.com/IamThiago-IT/repositorio-design-patterns.git
   ```

2. Instale as dependências necessárias:
   ```bash
   cd repositorio-design-patterns
   npm install 
   ```

3. Explore os exemplos e leia os comentários no código para entender como cada padrão funciona. Você pode executar scripts específicos para testar diferentes padrões:
   ```bash
   npm run  # yarn dev
   ```

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar mais exemplos ou corrigir algo, fique à vontade para abrir um Pull Request. Aqui está como contribuir:

1. Fork o repositório.
2. Crie um branch para a sua feature/correção:
   ```bash
   git checkout -b minha-nova-feature
   ```

3. Faça suas alterações e suba o branch:
   ```bash
   git push origin minha-nova-feature
   ```

4. Abra um Pull Request descrevendo suas mudanças.

## Licença

Este repositório está licenciado sob a [MIT License](./LICENSE). Sinta-se livre para usá-lo, modificá-lo e distribuí-lo conforme necessário.

## Agradecimentos

Obrigado por visitar este repositório! Se você tiver dúvidas ou sugestões, por favor, abra um issue ou entre em contato. Aproveite e bons estudos!