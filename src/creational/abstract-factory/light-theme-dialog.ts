import { Dialog } from './dialog';

export class LightThemeDialog implements Dialog {
  open(): void {
    console.log('Abrindo diálogo com tema claro');
  }
}