import { Dialog } from './dialog';

export class DarkThemeDialog implements Dialog {
  open(): void {
    console.log('Abrindo diálogo com tema escuro');
  }
}