import { Button } from './button';

export class DarkThemeButton implements Button {
  render(): void {
    console.log('Renderizando botão com tema escuro');
  }
}