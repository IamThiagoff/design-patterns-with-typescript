// theme-factory.ts
import { Button } from './button';
import { Dialog } from './dialog';

export interface ThemeFactory {
  createButton(): Button;
  createDialog(): Dialog;
}
