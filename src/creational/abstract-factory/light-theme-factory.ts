import { ThemeFactory } from './theme-factory';
import { Button } from './button';
import { Dialog } from './dialog';
import { LightThemeButton } from './light-theme-button';
import { LightThemeDialog } from './light-theme-dialog';

export class LightThemeFactory implements ThemeFactory {
  createButton(): Button {
    return new LightThemeButton();
  }

  createDialog(): Dialog {
    return new LightThemeDialog();
  }
}