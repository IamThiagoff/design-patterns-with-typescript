// main.ts
import { LightThemeFactory } from './light-theme-factory';
import { DarkThemeFactory } from './dark-theme-factory';
import { ThemeFactory } from './theme-factory';

// Simular uso do tema claro
let factory: ThemeFactory = new LightThemeFactory();
const button = factory.createButton();
const dialog = factory.createDialog();

button.render(); // Renderizando botão com tema claro
dialog.open(); // Abrindo diálogo com tema claro

// Simular uso do tema escuro
factory = new DarkThemeFactory();
const darkButton = factory.createButton();
const darkDialog = factory.createDialog();

darkButton.render(); // Renderizando botão com tema escuro
darkDialog.open(); // Abrindo diálogo com tema escuro
