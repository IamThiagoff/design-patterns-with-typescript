import { ThemeFactory } from './theme-factory';
import { Button } from './button';
import { Dialog } from './dialog';
import { DarkThemeButton } from './dark-theme-button';
import { DarkThemeDialog } from './dark-theme-dialog';

export class DarkThemeFactory implements ThemeFactory {
  createButton(): Button {
    return new DarkThemeButton();
  }

  createDialog(): Dialog {
    return new DarkThemeDialog();
  }
}