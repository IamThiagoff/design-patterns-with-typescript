# Exemplo de Abstract Factory com TypeScript

Este repositório contém um exemplo de uso do padrão de design Abstract Factory (Fábrica Abstrata) em TypeScript. O exemplo mostra como criar famílias de objetos relacionados sem especificar suas classes concretas, facilitando a manutenção e a expansão do sistema.

## O Que É Abstract Factory?

O padrão Abstract Factory é um padrão criacional que fornece uma interface para criar famílias de objetos relacionados ou dependentes sem especificar suas classes concretas. Isso permite ao sistema ser projetado de maneira mais flexível, facilitando alterações nas implementações de produtos sem modificar o código do cliente.

### Quando Usar Abstract Factory?

- Quando você precisa criar famílias de produtos relacionados ou dependentes.
- Quando você deseja garantir que os produtos em uma família sejam consistentes entre si.
- Quando você deseja ocultar as classes concretas dos produtos, fornecendo apenas uma interface para criação.

## Estrutura do Exemplo

Neste exemplo, temos uma aplicação gráfica com temas claros e escuros. Para cada tema, temos componentes como Botões e Caixas de Diálogo que precisam ser estilizados de acordo com o tema. A estrutura do código é a seguinte:

- **Interfaces de Produtos**: Definimos interfaces comuns para Botões (`Button`) e Diálogos (`Dialog`).
- **Implementações Concretas para Cada Tema**:
  - Botões e Diálogos com Tema Claro (`LightThemeButton`, `LightThemeDialog`).
  - Botões e Diálogos com Tema Escuro (`DarkThemeButton`, `DarkThemeDialog`).
- **Interface da Fábrica Abstrata**: A interface `ThemeFactory` define métodos para criar Botões e Diálogos.
- **Fábricas Concretas para Cada Tema**:
  - Fábrica para o Tema Claro (`LightThemeFactory`).
  - Fábrica para o Tema Escuro (`DarkThemeFactory`).

## Usando o Exemplo

Para usar este exemplo, siga os passos abaixo:

1. **Clone o Repositório**:
   ```bash
   git clone https://github.com/seu-usuario/exemplo-abstract-factory.git
   ```

2. **Instale as Dependências**:
   ```bash
   cd exemplo-abstract-factory
   npm install
   ```

3. **Execute o Código**:
   ```bash
   npx ts-node src/main.ts
   ```

4. **Resultados Esperados**:
   - O código criará Botões e Diálogos para o tema claro e para o tema escuro, exibindo mensagens apropriadas para cada caso.

