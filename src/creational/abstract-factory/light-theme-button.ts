import { Button } from './button';

export class LightThemeButton implements Button {
  render(): void {
    console.log('Renderizando botão com tema claro');
  }
}