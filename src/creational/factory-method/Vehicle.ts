// src/creational/factory-method/Vehicle.ts
export interface Vehicle {
    drive(): void;
  }
  