// src/creational/factory-method/Car.ts
import { Vehicle } from "./Vehicle";

export class Car implements Vehicle {
  drive() {
    console.log("Dirigindo um carro!");
  }
}
